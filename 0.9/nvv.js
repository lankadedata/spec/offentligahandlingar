rdforms.spec({
  language: 'sv',
  introduction: '<p>Syftet med denna metadataprofil är att hålla metadata om myndigheters handlingar (juridiskt' +
    ' perspektiv) eller' +
    ' dokument (tekniskt perspektiv) som ska tillgängliggöras. Själva handlingen/dokumentet' +
    ' tillgängliggörs inte i samband med metadatat utan kommer i de flesta fall att få begäras ut separat. Metadatat' +
    ' kan innehålla länkar till den plats där handlingen/dokumentet kan begäras ut/hämtas (i enlighet med' +
    ' beslutande myndighets policys)</p>' +
    '<p>Metadata om dokument förväntas innehålla tillräckligt med uppgifter så att tredje part, allmänheten' +
    ' m.fl. erbjuds "tillräcklig" sökbarhet. Ett sökgränsnitt för insamlade handlingar enligt denna' +
    ' specifikation finns att beskåda på naturvårdsverkets söktjänst för <a' +
    ' href="https://metadatasok.naturvardsverket.se"' +
    ' target="blank">dokument om miljöfarliga verksamheter</a>.</p>' +
    '<p>Denna specifikation anger vilka fält som är obligatoriska. T.ex. gäller att man måste tillhandahålla <a' +
    ' href="#nvvdok_Document-dcterms_title">titel</a>,' +
    ' <a href="#nvvdok_Document-dcterms_description">beskrivning</a> och <a' +
    ' href="#nvvdok_Document-nvvdok_documentType">dokumenttyp</a>. När det gäller' +
    ' dokumentundertyperna (besluts, rapport, ansökan, anmälan, överklagande, plan och referenstyp) gäller att' +
    ' de är villkorsbundet obligatoriska. Endast den undertyp som överenstämmer med huvudtypen ska anges (obligatorisk),' +
    ' de andra undertyperna ska inte anges.</p>' +
    '<p>Denna specifikation är uttryckt som en applikations profil i <a href="https://www.w3.org/RDF/">RDF</a>.' +
    ' När så är möjligt har existerande vokabulärer återanvänts, endast när' +
    ' ingen lämplig egenskap (property) har kunnat identifierats har nya egenskaper införts.</p>' +
    '<p>I appendix A finns ett exempel på hur en handling uttrycks i RDF. Det finns ett flertal olika syntaxer som' +
    ' går att använda för att uttrycka RDF. I appendix A används <a href="https://www.w3.org/TR/turtle/">Turtle</a> då' +
    ' den är någorlunda kompakt och är relativt enkel att läsa. I appendix B redovisas en översättning från ett XML' +
    ' uttryck till metadataprofilen.</p>' +
    '<p><strong>Metadata om dokument som uttrycks i enlighet med denna specifikation kan skördas till en nationell' +
    ' databas som förvaltas av Naturvårdsverket.</strong></p>',
  bundles: [
    ['./dcterms.json'],
    ['./skos.json'],
    ['./adms.json'],
    ['./foaf.json'],
    ['./vcard.json'],
    ['./dcat_props.json'],
    ['./dcat.json'],
    ['./dc.json'],
    ['./schema.json'],
    ['./nvv.json']
  ],
  main: [
    'nvvdok:Document',
    'nvvdok:org:Organization'
  ],
  supportive: [
  ]
});