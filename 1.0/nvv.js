rdforms.spec({
  language: 'sv',
  introduction: '<p>Syftet med denna metadataprofil är att dokumentera och tillgängliggöra information/metadata om' +
    ' myndigheters handlingar (juridiskt perspektiv) eller' +
    ' dokument (tekniskt perspektiv). Metadataprofilen/tillgängliggörandet omfattar inte myndighetspublikationer ' +
    ' (som dokumenteras i DiVA) eller datamängder (som dokumenteras i Naturvårdsverkets centrala metadatakatalog.' +
    ' Själva handlingen/dokumentet' +
    ' tillgängliggörs inte tillsammans med metadatat utan kommer i de flesta fall att få begäras ut separat. Metadatat' +
    ' kan innehålla länkar till den plats där handlingen/dokumentet kan begäras ut/hämtas (i enlighet med' +
    ' beslutande myndighets policy).</p>' +
    '<p>Metadata om dokument förväntas innehålla tillräckligt med uppgifter för att den som behöver få tag på ett' +
    ' myndighetsdokument ska kunna hitta det. En söktjänst för insamlade handlingar enligt denna' +
    ' specifikation finns på Naturvårdsverkets hemsida för <a' +
    ' href="https://metadatasok.naturvardsverket.se"' +
    ' target="blank">dokument om miljöfarliga verksamheter</a>.</p>' +
    '<p>Denna specifikation anger vilka fält som är obligatoriska. T.ex. gäller att man måste ange <a' +
    ' href="#nvvdok_Document-dcterms_title">titel</a>,' +
    ' <a href="#nvvdok_Document-dcterms_description">beskrivning</a> och <a' +
    ' href="#nvvdok_Document-nvvdok_documentType">dokumenttyp</a>. När det gäller' +
    ' dokumentundertyperna (beslut, rapport som ej är myndighetspublikation, ansökan, anmälan, överklagande, plan och referenstyp) gäller att' +
    ' de är villkorsbundet obligatoriska. Endast den undertyp som stämmer överens med huvudtypen ska anges (obligatorisk),' +
    ' de andra undertyperna ska inte anges.</p>' +
    '<p>Denna specifikation är uttryckt som en applikationsprofil i <a href="https://www.w3.org/RDF/">RDF</a>.' +
    ' När så är möjligt har existerande vokabulärer återanvänts. Endast när' +
    ' ingen lämplig egenskap (property) har kunnat identifieras har nya egenskaper införts.</p>' +
    '<p>I appendix A finns ett exempel på hur en handling uttrycks i RDF. Det finns ett flertal olika syntaxer som' +
    ' går att använda för att uttrycka RDF. I appendix A används <a href="https://www.w3.org/TR/turtle/">Turtle</a> då' +
    ' den är någorlunda kompakt och är relativt enkel att läsa. I appendix B redovisas en översättning från ett XML' +
    ' uttryck till metadataprofilen.</p>' +
    '<p><strong>Metadata om dokument som uttrycks i enlighet med denna specifikation kan skördas till en nationell' +
    ' databas som förvaltas av Naturvårdsverket.</strong></p>',
  bundles: [
    ['./dcterms.json'],
    ['./skos.json'],
    ['./adms.json'],
    ['./foaf.json'],
    ['./vcard.json'],
    ['./dcat_props.json'],
    ['./dcat.json'],
    ['./dc.json'],
    ['./schema.json'],
    ['./nvv.json']
  ],
  main: [
    'nvvdok:Document',
    'nvvdok:org:Organization'
  ],
  supportive: [
  ]
});